package main

import (
	"fmt"
	"io/fs"
	"io/ioutil"
	"os"
	//  "strings"
)

var sep string = string(os.PathSeparator)

type directory struct {
	name string
	path string
	position int
	countPositions int
	childs []fs.FileInfo
	parrent *directory
}

func (d *directory) getChilds(printFiles bool) []fs.FileInfo {
	path, _ := os.Getwd()
	childs, _ := ioutil.ReadDir(path + sep + d.path)
	if printFiles {
		d.childs = childs
	} else {
		for i := 0; i < len(childs); i++{
			if childs[i].IsDir() {
				d.childs = append(d.childs, childs[i])
			}
		}
	}
	return d.childs
}
func (d *directory) getCountPositions() int {
	if(d.parrent != nil){
		d.countPositions = len(d.parrent.childs)
		return d.countPositions
	}
	return 0
}

func getPrefix(dir directory) (prefix string){
	fordir := dir
	for fordir.parrent.parrent != nil {
		prefix += "|\t"
		fordir = *fordir.parrent
	}
	if(dir.position < dir.countPositions - 1){
		prefix +=  "├" + "───"
	} else {
		prefix +=  "└" + "───"
	}
	return
}

func dirTree(dir *directory, printFiles bool) {
	dir.getChilds(printFiles)
	for pos, child := range dir.childs{
		var newDir directory = directory{name: child.Name(), path: dir.path + sep + child.Name(), position: pos, parrent: dir}
		newDir.getCountPositions()
		fmt.Printf("%v of %v. %v%v\n", newDir.position, newDir.countPositions,getPrefix(newDir), newDir.name)
		dirTree(&newDir, printFiles)
	}
}

func main() {
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}

	dir := directory{path: os.Args[1], parrent: nil, position: 0, countPositions: 2, name: "hw1_tree"}
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	dirTree(&dir, printFiles)
}
